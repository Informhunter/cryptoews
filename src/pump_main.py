from data.pumps import BittrexToRedisDataPump
from secret import bittrex_api_secret, bittrex_api_token
import redis


def main():
    r = redis.StrictRedis(host='127.0.0.1', port=6379)
    bittrex_pump = BittrexToRedisDataPump(r, bittrex_api_token, bittrex_api_secret, [
        'USDT-BTC',
        'USDT-ETH',
        'USDT-LTC',
        'USDT-XRP',
    ])
    bittrex_pump.start_pumping(join=True)


if __name__ == '__main__':
    main()
