from handlers import HandlerBase


class EventDeliveryHandler(HandlerBase):

    def __init__(self, message_builders, deliverers):
        self.message_builders = message_builders
        self.deliverers = deliverers

    def handle(self, event):
        if len(self.message_builders) == 1:
            msg = self.message_builders[0].build_message(event)
            for deliverer in self.deliverers:
                deliverer.send_message(msg)
        else:
            for builder, deliverer in zip(self.message_builders, self.deliverers):
                msg = builder.build_message(event)
                deliverer.send_message(msg)
