from abc import ABC, abstractmethod


class HandlerBase(ABC):

    @abstractmethod
    def handle(self, event):
        pass
