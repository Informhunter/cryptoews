import time
from triggers import TriggerBase
from data.datamanager import DataManager
from events import LevelCrossedEvent


class Level:
    def __init__(self, value):
        self.value = value
        self.triggered_timestamp = 0
        self.trigger_timeout = 60

    def crossed(self, a, b):
        return min(a, b) <= self.value <= max(a, b)

    def can_trigger(self):
        timestamp = time.time()
        time_passed = timestamp - self.triggered_timestamp
        print(time_passed)
        return time_passed > self.trigger_timeout

    def trigger(self):
        self.triggered_timestamp = time.time()


class LevelTrigger(TriggerBase):

    def __init__(self, data_manager: DataManager, key: str):
        super().__init__()
        self.data_manager = data_manager
        self.levels = []
        self.key = key
        self.last_trigger_timestamp = 0

    def add_level(self, level):
        self.levels.append(Level(level))

    def update(self):
        data = self.data_manager.get_data([self.key,
                                           self.key + ':timestamps'])
        timestamps = data[self.key + ':timestamps']
        data = data[self.key]
        if self.last_trigger_timestamp == 0:
            self.last_trigger_timestamp = float(timestamps[-1])

        for level in self.levels:
            look_backwards = 0
            for t in timestamps:
                if float(t) <= self.last_trigger_timestamp:
                    break
                look_backwards += 1
            for i in range(look_backwards - 1):
                #Indices go now -> past
                a = float(data[i+1])
                b = float(data[i])
                if level.crossed(a, b):
                    if level.can_trigger():
                        level.trigger()
                        event = LevelCrossedEvent(
                            0,
                            self.key,
                            level.value,
                            a,
                            b
                        )
                        self.trigger(event)
                    self.last_trigger_timestamp = float(timestamps[i])

    def get_name(self):
        return 'LevelTrigger'

