class TriggerGroup:

    def __init__(self, triggers=[]):
        self.triggers = {}
        for trigger in triggers:
            self.triggers[trigger.get_name()] = trigger

    def add_trigger(self, trigger):
        self.triggers[trigger.get_name()] = trigger

    def update(self):
        for _, trigger in self.triggers.items():
            trigger.update()
