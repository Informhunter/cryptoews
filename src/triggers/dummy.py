from triggers import TriggerBase
from data.datamanager import DataManager


class DummyTrigger(TriggerBase):
    def __init__(self, data_manager: DataManager, key='dummy_data'):
        super().__init__()
        self.data_manager = data_manager
        self.handlers = []
        self.key = key

    def update(self):
        keys = [self.key]
        data = self.data_manager.get_data(keys)
        print(data[self.key][0])

        # if int(data[self.key]) % 10 == 0:
        #     self.trigger(data[self.key])

    def get_name(self):
        return 'DummyTrigger'
