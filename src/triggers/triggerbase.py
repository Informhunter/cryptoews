from abc import ABC, abstractmethod


class TriggerBase(ABC):

    def __init__(self):
        self.handlers = []

    def add_handler(self, handler):
        self.handlers.append(handler)

    def trigger(self, event):
        for handler in self.handlers:
            handler.handle(event)

    @abstractmethod
    def update(self):
        pass
