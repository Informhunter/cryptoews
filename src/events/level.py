class LevelCrossedEvent:

    def __init__(self, timestamp, stat, level, start, end):
        self.timestamp = timestamp
        self.stat = stat
        self.level = level
        self.start = start
        self.end = end
