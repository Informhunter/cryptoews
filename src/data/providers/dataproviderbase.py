from abc import abstractmethod, ABC


class DataProviderBase(ABC):

    @abstractmethod
    def get_keys(self):
        pass

    @abstractmethod
    def retrieve_data(self, key):
        pass
