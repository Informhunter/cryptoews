from data.providers import DataProviderBase


class CountingDataProvider(DataProviderBase):

    def __init__(self):
        self.data = {
            'dummy_data': 0,
        }

    def retrieve_data(self, key):
        self.data['dummy_data'] += 1
        return self.data[key]

    def get_keys(self):
        return self.data.keys()
