from data.providers import DataProviderBase


class MiddleDataProvider(DataProviderBase):

    def __init__(self, data_manager, key1, key2):
        self.data_manager = data_manager
        self.key1 = key1
        self.key2 = key2
        self.dependencies = [
            self.key1,
            self.key2,
        ]
        self.output_key = self.key1 + '_' + self.key2 + '_mid'
        print(self.output_key)

    def get_keys(self):
        return [self.output_key]

    def retrieve_data(self, key):
        data = self.data_manager.get_data(self.dependencies)
        a = data[self.key1]
        b = data[self.key2]
        return (a + b) / 2
