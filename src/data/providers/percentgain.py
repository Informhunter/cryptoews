from data.providers import DataProviderBase


class PercentGainDataProvider(DataProviderBase):
    def __init__(self, data_manager, key, window, look_back_period):
        self.data_manager = data_manager
        self.key = key
        self.window = window
        self.look_back_period = look_back_period
        self.output_key = '{}_percent_gain_{}'.format(key, window)
        self.dependencies = [
            self.key,
            self.key + ':timestamps'
        ]

    def get_keys(self):
        return [self.output_key]

    def get_dependencies(self):
        return self.dependencies

    def retrieve_data(self, key):
        data = self.data_manager.get_data(self.dependencies)
        timestamps = data[self.key + ':timestamps']
        data = data[self.key]
        res = []
        start = 0
        end = 0
        start_sum = 0
        print(timestamps)
        while start_sum < self.look_back_period:
            end_sum = float(timestamps[start]) - float(timestamps[end])
            while end_sum < self.window and end < len(timestamps):
                end += 1
                end_sum = float(timestamps[start]) - float(timestamps[end])
            start += 1
            start_sum = float(timestamps[0]) - float(timestamps[start])
            a = float(data[start])
            b = float(data[end])
            print(end, start)
            print(a, b)
            res.append((b / a - 1.0) * 100.0)
        return res
