from data.providers import DataProviderBase
from redis import StrictRedis
import numpy as np


class RedisDataProvider(DataProviderBase):
    def __init__(self, redis: StrictRedis, keys=[]):
        self.redis = redis
        self.keys = keys

    def get_keys(self):
        return self.keys

    def retrieve_data(self, key):
        t = self.redis.type(key)
        if t == b'list':
            return np.array(list(map(float, self.redis.lrange(key, 0, -1))))
        elif t == b'string':
            return float(self.redis.get(key))
        else:
            return None
