from data.providers.dataproviderbase import DataProviderBase
from data.providers.dummy import CountingDataProvider
from data.providers.redis import RedisDataProvider
from data.providers.percentgain import PercentGainDataProvider
from data.providers.middle import MiddleDataProvider
