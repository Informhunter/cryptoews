class DataManager:
    def __init__(self, data_providers=[]):
        self.data_providers = {}
        self.data_processors = {}
        self.data_cache = {}
        self.data_processor_cache = {}

        for data_provider in data_providers:
            self.add_data_provider(data_provider)

    def add_data_provider(self, data_provider):
        keys = data_provider.get_keys()
        for key in keys:
            self.data_providers[key] = data_provider

    def add_data_processor(self, data_processor):
        keys = data_processor.get_keys()
        for key in keys:
            self.data_processors[key] = data_processor

    def get_data(self, keys=[]):
        result = {}
        for key in keys:
            if key in self.data_providers:
                result[key] = self.data_cache[key]
            elif key in self.data_processors:
                if key not in self.data_processor_cache:
                    self.data_processor_cache[key] = self.data_processors[key].retrieve_data(key)
                result[key] = self.data_processor_cache[key]
        return result

    def retrieve_data(self):
        self.data_processor_cache = {}
        self.data_cache = {}
        for key in self.data_providers.keys():
            self.data_cache[key] = self.data_providers[key].retrieve_data(key)
