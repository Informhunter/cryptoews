import time
from threading import Thread
from bittrex.bittrex import Bittrex, API_V1_1


class BittrexToRedisDataPump:
    def __init__(self, redis, api_token, api_secret, tickers=[]):
        self.redis = redis
        self.btx = Bittrex(api_token, api_secret, api_version=API_V1_1)
        self.pumping_thread = None
        self.pumping_enabled = False
        self.tickers = tickers

    def start_pumping(self, join=False):
        self.pumping_enabled = True
        self.pumping_thread = Thread(target=self.pumping_loop)
        self.pumping_thread.start()
        if join:
            self.pumping_thread.join()

    def stop_pumping(self):
        if self.pumping_enabled and self.pumping_thread:
            self.pumping_enabled = False
        self.pumping_thread.join()

    def pumping_loop(self):
        while self.pumping_enabled:
            self.__fetch_data()
            self.__cut_data_lists()
            time.sleep(1)

    def __fetch_data(self):
        for ticker in self.tickers:
            ticker_info = self.btx.get_ticker(ticker)
            timestamp = time.time()
            prefix = ticker.lower() + '_'
            with self.redis.pipeline() as p:
                p.lpush(prefix + 'bid', ticker_info['result']['Bid'])
                p.lpush(prefix + 'bid' + ':timestamps', timestamp)

                p.lpush(prefix + 'ask', ticker_info['result']['Ask'])
                p.lpush(prefix + 'ask' + ':timestamps', timestamp)

                p.lpush(prefix + 'last', ticker_info['result']['Last'])
                p.lpush(prefix + 'last' + ':timestamps', timestamp)

                p.execute()

    def __cut_data_lists(self):
        for ticker in self.tickers:
            prefix = ticker.lower() + '_'
            with self.redis.pipeline() as p:
                p.ltrim(prefix + 'bid', -3600, -1)
                p.ltrim(prefix + 'bid' + ':timestamp', -3600, -1)

                p.ltrim(prefix + 'ask', -3600, -1)
                p.ltrim(prefix + 'ask' + ':timestamp', -3600, -1)

                p.ltrim(prefix + 'last', -3600, -1)
                p.ltrim(prefix + 'last' + ':timestamp', -3600, -1)

                p.execute()
