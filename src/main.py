import time
import redis
import telegram
import matplotlib.pyplot as plt
from comm import DelivererGroup
from comm.deliverers import TelegramMessageDeliverer
from data import DataManager
from data.providers import RedisDataProvider, PercentGainDataProvider, MiddleDataProvider
from secret import *
from triggers import DummyTrigger, LevelTrigger, TriggerGroup
from handlers import EventDeliveryHandler
from comm.messagebuilders import LevelMessageBuilder


def main():
    r = redis.StrictRedis(host='127.0.0.1', port=6379)

    bot = telegram.Bot(telegram_access_token)

    chat = TelegramMessageDeliverer(bot, 107722982)
    channel_group = DelivererGroup([chat])

    data_manager = DataManager([RedisDataProvider(r, [
        'usdt-btc_bid', 'usdt-btc_bid:timestamps',
        'usdt-eth_bid', 'usdt-eth_bid:timestamps',
        'usdt-ltc_bid', 'usdt-ltc_bid:timestamps',
        'usdt-xrp_bid', 'usdt-xrp_bid:timestamps',
    ])])

    data_manager.retrieve_data()

    data_manager.add_data_processor(PercentGainDataProvider(data_manager, 'usdt-btc_bid', 300, 300))
    # data_manager.add_data_provider(MiddleDataProvider(data_manager, 'btc-bcc_bid', 'btc-bcc_ask'))
    #data_manager.add_data_provider(PercentGainDataProvider(data_manager, 'btc-bcc_bid_btc-bcc_ask_mid', 300, 1000))
    # data_manager.add_data_provider(PercentGainDataProvider(data_manager, 'btc-ltc_bid', 300, 300))
    # data_manager.add_data_provider(PercentGainDataProvider(data_manager, 'btc-dash_bid', 300, 300))


    data = data_manager.get_data(['usdt-btc_bid', 'usdt-btc_bid_percent_gain_300'])
    print(data.keys())
    print(data['usdt-btc_bid'])

    plt.figure()
    plt.plot(data['usdt-btc_bid'])
    plt.figure()
    plt.plot(data['usdt-btc_bid_percent_gain_300'])

    plt.show()


    # level_trigger = LevelTrigger(data_manager, 'btc-eth_bid')
    # level_trigger.add_level(0.0455)
    # level_trigger.add_level(0.0456)
    # level_trigger.add_handler(EventDeliveryHandler([LevelMessageBuilder()], [chat]))
    #
    # # dummy_trigger = DummyTrigger(data_manager, 'btc-xrp_last')
    # # dummy_trigger.subscribe(chat)
    #
    # trigger_group = TriggerGroup([level_trigger])
    #
    # while True:
    #     trigger_group.update()
    #     channel_group.update()
    #     time.sleep(1)

if __name__ == '__main__':
    main()
