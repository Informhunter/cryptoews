from telegram import Bot
from comm import Message


class TelegramMessageDeliverer:

    def __init__(self, bot: Bot, chat_id: int):
        self.bot = bot
        self.chat_id = chat_id
        self.outgoing_messages = []

    def send_message(self, msg: Message):
        self.outgoing_messages.append(msg)

    def update(self):
        if self.outgoing_messages:
            m = "\n".join(str(msg) for msg in self.outgoing_messages)
            self.bot.send_message(self.chat_id, m)
            self.outgoing_messages = []
