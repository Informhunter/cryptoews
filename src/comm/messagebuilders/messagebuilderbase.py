from abc import ABC, abstractmethod


class MessageBuilderBase(ABC):

    @abstractmethod
    def build_message(self, event):
        pass
