from comm.messagebuilders import MessageBuilderBase
from comm import Message

message_template = 'Timestamp: {}\n{} has crossed level {}\nFrom {} to {} {}'


class LevelMessageBuilder(MessageBuilderBase):

    def __init__(self):
        pass

    def build_message(self, event):
        text = message_template.format(
            event.timestamp,
            event.stat,
            event.level,
            event.start,
            event.end,
            '🔼' if event.start < event.end else '🔽'
        )
        return Message(text)
