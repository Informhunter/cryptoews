class DelivererGroup:
    def __init__(self, channels=[]):
        self.channels = channels

    def add_channel(self, channel):
        self.channels.append(channel)

    def update(self):
        for channel in self.channels:
            channel.update()
